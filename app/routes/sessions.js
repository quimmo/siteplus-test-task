const express = require('express');
const wrapper = require('../middlewares/routeWrapper');
const sessionsController = require('../controllers/sessionsController');

const router = express.Router();

router.post('/create', wrapper(async req => sessionsController.create(req.body)));

module.exports = router;
