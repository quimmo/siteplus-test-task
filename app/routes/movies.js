const express = require('express');
const wrapper = require('../middlewares/routeWrapper');
const moviesController = require('../controllers/moviesController');

const router = express.Router();

router.get('/', wrapper(async () => moviesController.index()));
router.post('/create', wrapper(async req => moviesController.create(req.body)));

module.exports = router;
