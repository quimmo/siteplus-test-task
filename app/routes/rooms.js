const express = require('express');
const wrapper = require('../middlewares/routeWrapper');
const roomsController = require('../controllers/roomsController');

const router = express.Router();

router.post('/create', wrapper(async req => roomsController.create(req.body)));
router.get('/:id/sessions/', wrapper(async req => roomsController.getSessions(req.params.id, req.query.day)));

module.exports = router;
