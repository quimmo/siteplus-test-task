const express = require('express');
const wrapper = require('../middlewares/routeWrapper');
const theatersController = require('../controllers/theatersController');

const router = express.Router();

router.get('/', wrapper(async () => theatersController.index()));
router.post('/create', wrapper(async req => theatersController.create(req.body)));
router.get('/:id/rooms/', wrapper(async req => theatersController.getRooms(req.params.id)));
router.get('/:id/sessions/', wrapper(async req => theatersController.getSessions(req.params.id, req.query.day)));

module.exports = router;
