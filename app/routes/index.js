const movies = require('./movies');
const theaters = require('./theaters');
const rooms = require('./rooms');
const sessions = require('./sessions');

module.exports = (app) => {
  app.use('/movies', movies);
  app.use('/theaters', theaters);
  app.use('/rooms', rooms);
  app.use('/sessions', sessions);
};

