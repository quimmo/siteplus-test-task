const bluebird = require('bluebird');
const Session = require('../dataAccess/models/session');
const CriticalError = require('../utils/criticalError');
const config = require('../utils/config');

const find = bluebird.promisify(Session.find, { context: Session });

const {
  ADD_SESSION_OK,
  ADD_SESSION_FAIL,
  DATABASE_ERROR,
} = require('../responses/responses');

class SessionsController {
  async create({ roomId, movieId, startTime }) {
    let busySessions;

    try {
      busySessions = await find({ roomId, movieId, startTime: startTime + config.movieBreak });
      if (busySessions.length) {
        return ADD_SESSION_FAIL;
      }
    } catch (error) {
      throw new CriticalError(Object.assign({}, DATABASE_ERROR, { message: error.message }));
    }

    const room = new Session({ roomId, movieId, startTime });
    room.save();
    return ADD_SESSION_OK;
  }
}

module.exports = new SessionsController();
