const bluebird = require('bluebird');

const Room = require('../dataAccess/models/room');
const CriticalError = require('../utils/criticalError');
const Session = require('../dataAccess/models/session');

const findSession = bluebird.promisify(Session.find, { context: Session });

const {
  ADD_ROOM_OK,
  DATABASE_ERROR,
  GET_SESSION_OK,
} = require('../responses/responses');

class RoomsController {
  create({ name, theaterId }) {
    const room = new Room({ name, theaterId });
    room.save();
    return ADD_ROOM_OK;
  }

  async getSessions(roomId, day) {
    const start = new Date(day).getTime();
    const end = start + 86400000;

    try {
      const sessions = await findSession({
        roomId,
        startTime: { $gte: start, $lte: end },
      });
      return Object.assign({}, GET_SESSION_OK, { payload: sessions });
    } catch (error) {
      throw new CriticalError(Object.assign({}, DATABASE_ERROR, { message: error.message }));
    }
  }
}

module.exports = new RoomsController();
