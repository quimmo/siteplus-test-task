const Movie = require('../dataAccess/models/movie');
const CriticalError = require('../utils/criticalError');

const {
  DATABASE_ERROR,
  GET_MOVIES_OK,
  ADD_MOVIE_OK,
} = require('../responses/responses');

class MoviesController {
  async index() {
    return new Promise((resolve, reject) => {
      Movie.find({}, (error, movies) => {
        if (error) {
          reject(new CriticalError(Object.assign({}, DATABASE_ERROR, { message: error.message })));
        }
        resolve(Object.assign({}, GET_MOVIES_OK, { payload: movies }));
      });
    });
  }

  async create({ name, duration, year }) {
    const movie = new Movie({ name, duration, year });
    movie.save();
    return ADD_MOVIE_OK;
  }
}

module.exports = new MoviesController();
