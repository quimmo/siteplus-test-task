const bluebird = require('bluebird');
const _ = require('lodash');

const Theater = require('../dataAccess/models/theater');
const CriticalError = require('../utils/criticalError');
const Room = require('../dataAccess/models/room');
const Session = require('../dataAccess/models/session');

const findSession = bluebird.promisify(Session.find, { context: Session });
const findRoom = bluebird.promisify(Room.find, { context: Room });

const {
  DATABASE_ERROR,
  ADD_THEATER_OK,
  GET_THEATERS_OK,
  GET_ROOMS_OK,
  GET_SESSION_OK,
} = require('../responses/responses');

class TheatersController {
  index() {
    return new Promise((resolve, reject) => {
      Theater.find({}, (error, theaters) => {
        if (error) {
          reject(new CriticalError(Object.assign({}, DATABASE_ERROR, { message: error.message })));
        }
        resolve(Object.assign({}, GET_THEATERS_OK, { payload: theaters }));
      });
    });
  }

  create({ name, city }) {
    const movie = new Theater({ name, city });
    movie.save();
    return ADD_THEATER_OK;
  }

  getRooms(theaterId) {
    return new Promise((resolve, reject) => {
      Room.find({ theaterId }, (error, rooms) => {
        if (error) {
          reject(new CriticalError(Object.assign({}, DATABASE_ERROR, { message: error.message })));
        }
        resolve(Object.assign({}, GET_ROOMS_OK, { payload: rooms }));
      });
    });
  }

  async getSessions(theaterId, day) {
    const start = new Date(day).getTime();
    const end = start + 86400000;

    try {
      const rooms = await findRoom({ theaterId });
      const sessions = await Promise.all(rooms.map(obj => findSession({
        roomId: obj._id, // eslint-disable-line
        startTime: { $gte: start, $lte: end },
      })));
      return Object.assign({}, GET_SESSION_OK, { payload: _.flatten(sessions) });
    } catch (error) {
      console.log(error);
      throw new CriticalError(Object.assign({}, DATABASE_ERROR, { message: error.message }));
    }
  }
}

module.exports = new TheatersController();
