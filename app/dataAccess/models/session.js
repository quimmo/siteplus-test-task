const Model = require('./model');

class Session extends Model {
  constructor() {
    const name = 'Session';
    const schema = {
      roomId: String,
      movieId: String,
      startTime: Number,
    };
    super(name, schema);
  }
}

module.exports = (new Session()).model;
