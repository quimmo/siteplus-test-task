const Model = require('./model');

class Theater extends Model {
  constructor() {
    const name = 'Theater';
    const schema = {
      name: String,
      city: String,
    };
    super(name, schema);
  }
}

module.exports = (new Theater()).model;
