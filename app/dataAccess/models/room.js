const Model = require('./model');

class Room extends Model {
  constructor() {
    const name = 'Room';
    const schema = {
      name: String,
      seats: Number,
      theaterId: String,
    };
    super(name, schema);
  }
}

module.exports = (new Room()).model;
