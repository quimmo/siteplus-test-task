const Model = require('./model');

class Movie extends Model {
  constructor() {
    const name = 'Movie';
    const schema = {
      name: String,
      duration: Number,
      year: Number,
    };
    super(name, schema);
  }
}

module.exports = (new Movie()).model;
