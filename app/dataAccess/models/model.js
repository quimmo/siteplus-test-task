const mongoose = require('mongoose');
const config = require('../../utils/config');
const CriticalError = require('../../utils/criticalError');

const {
  DATABASE_ERROR,
} = require('../../responses/responses');

class Model {
  constructor(name = '', schema = {}) {
    this.connect();
    this.name = name;
    this.schema = schema;
    if (!this.model) {
      this.model = this.createModel(this.name, this.schema);
    }
  }

  createModel(name, fields) {
    const schema = mongoose.Schema(fields);
    schema.methods = this.getFunctions();
    return mongoose.model(name, schema);
  }

  async connect() {
    try {
      const options = {
        user: config.mongodb.user,
        pass: config.mongodb.pass,
        dbName: config.mongodb.database,
      };
      await mongoose.connect(`mongodb://${config.mongodb.connection}`, options);
    } catch (error) {
      throw new CriticalError(Object.assign({}, DATABASE_ERROR, { message: 'Cant connect' }));
    }
  }

  getFunctions() {
    return Object.getOwnPropertyNames(Object.getPrototypeOf(this))
      .filter(func => func !== 'constructor')
      .map(key => this[key]);
  }
}

module.exports = Model;
