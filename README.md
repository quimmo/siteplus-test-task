# Siteplus test task

### Test task for siteplus. ###
*** Cinemas "CRM" API ***   
   
- REST API
- Node.JS, Express, Mongo.
- Running on Docker.

*** Build process ***

- Run ```sudo bash build.sh``` in 'app' directory.
- Run ```sudo docker-compose up``` in docker directory.
